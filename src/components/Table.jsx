import React, {useState} from 'react';
import Cell from './Cell';

function Row(props){

	return (
		<div className = 'row'>
		{
			props.cellId.map(cellId => 
				<Cell cellId = {cellId}
					a = {props.a} 
					setLastAction = {props.setLastAction}
					lastAction = {props.lastAction}
					arr = {props.arr}
					setHeader = {props.setHeader}/>
			)
		}
		</div>
	);
}


export default Row;